import java.util.HashMap;

public class NoteBookTree {
    private Node _root;
    private Node _currentNode;

    public NoteBookTree() {
        _root = new Node(new HashMap<>(), null);
        _currentNode = _root;
        _root.setNumberOfChildren(0);
    }

    public void addNode(Character ch) {
        //if key isset go to next
        if(_currentNode.getChildren().containsKey(ch)) {
            goDownByCH(ch);
            return;
        }
        //create new node
        Node newNode = new Node(new HashMap<>(), _currentNode);
        //notify parents about new node
        Node parent = _currentNode;
        if(parent.getNumberOfChildren() == 0) {
            parent.setNumberOfChildren(parent.getNumberOfChildren() + 1);
        }else {
            do {
                parent.setNumberOfChildren(parent.getNumberOfChildren() + 1);
                parent = parent.getParent();
            } while (parent != null);
        }


        _currentNode.getChildren().put(ch, newNode);//put to parent list
        _currentNode = newNode;
    }

    public Number getNumberOfChildren() {
        return _currentNode.getNumberOfChildren();
    }

    public boolean goDownByCH(Character ch) {
        Node currentNode = _currentNode.getChildren().get(ch);
        if(currentNode == null) {
            return false;
        }
        _currentNode = currentNode;
        return true;
    }


    public void resetCur() {
        _currentNode = _root;
    }
}
