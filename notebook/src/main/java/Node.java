import java.util.Map;

public class Node {
    private Map<Character, Node> _children;
    private Node _parent;
    private Integer _numberOfChildren = 0;

    public Node (Map<Character, Node> children, Node parent) {
        _children = children;
        _parent = parent;
    }

    public Map<Character, Node> getChildren() {
        return _children;
    }

    public Node getParent() {
        return _parent;
    }

    public Integer getNumberOfChildren() {
        return _numberOfChildren;
    }

    public void setNumberOfChildren(Integer numberOfChildren) {
        this._numberOfChildren = numberOfChildren;
    }
}