public class NoteBook {

    NoteBookTree _contacts = new NoteBookTree();

    public void addContact(String name) {
        for (Character ch : name.toCharArray()) {
            _contacts.addNode(ch);
        }
        _contacts.resetCur();
    }

    public Number findContactsWithPartOfName(String partOfName) {
        for (Character ch : partOfName.toCharArray()) {

            if (!_contacts.goDownByCH(ch)) {
                return 0;
            }
        }
        return _contacts.getNumberOfChildren();
    }
}
