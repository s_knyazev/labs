import org.junit.Test;
import org.testng.Assert;

public class testNotebook {
    @Test
    public void testFindContactsFullCoincidence() {
        NoteBook noteBook = new NoteBook();
        noteBook.addContact("alexey");
        noteBook.addContact("alexander");

        Assert.assertEquals(noteBook.findContactsWithPartOfName("alex"), 2);
    }

    @Test
    public void testFindContactsWithNoCoincidence() {
        NoteBook noteBook = new NoteBook();
        noteBook.addContact("alexey");
        noteBook.addContact("alexander");

        Assert.assertEquals(noteBook.findContactsWithPartOfName("alek"), 0);
    }
}
