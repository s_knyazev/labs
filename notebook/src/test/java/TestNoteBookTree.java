import org.junit.Test;
import org.testng.Assert;

public class TestNoteBookTree {

    @Test
    public void testAddAndGetNode() {
        NoteBookTree noteBookTree = new NoteBookTree();
        noteBookTree.addNode('a');
        noteBookTree.addNode('b');
        noteBookTree.addNode('c');
        noteBookTree.resetCur();
        Assert.assertEquals(noteBookTree.getNumberOfChildren(), 1);

        noteBookTree.addNode('q');
        noteBookTree.addNode('w');
        noteBookTree.resetCur();
        Assert.assertEquals(noteBookTree.getNumberOfChildren(), 2);

        noteBookTree.goDownByCH('a');
        noteBookTree.goDownByCH('b');
        Assert.assertEquals(noteBookTree.getNumberOfChildren(), 1);
    }

    @Test
    public void testAddAndGetNodeReal() {
        NoteBookTree noteBookTree = new NoteBookTree();
        noteBookTree.addNode('a');
        noteBookTree.addNode('l');
        noteBookTree.addNode('e');
        noteBookTree.addNode('x');
        noteBookTree.addNode('e');
        noteBookTree.addNode('y');
        noteBookTree.resetCur();
        Assert.assertEquals(noteBookTree.getNumberOfChildren(), 1);

        noteBookTree.goDownByCH('a');
        noteBookTree.goDownByCH('l');
        noteBookTree.goDownByCH('e');
        noteBookTree.goDownByCH('x');
        noteBookTree.addNode('a');
        noteBookTree.resetCur();
        Assert.assertEquals(noteBookTree.getNumberOfChildren(), 2);
    }
}
