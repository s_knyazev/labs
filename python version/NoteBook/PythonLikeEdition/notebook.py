
class NoteBook(object):
    name_list = []

    def add(self, name):
        self.name_list.append(name.lower())

    def find(self, name):
        name = name.lower()
        i = 0
        for current_name in self.name_list:
            if name in current_name:
                i += 1
        return i
