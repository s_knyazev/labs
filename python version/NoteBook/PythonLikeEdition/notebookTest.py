import unittest

from src.notebook import NoteBook


class TestNoteBook(unittest.TestCase):

    @unittest.expectedFailure
    def test_broken(self):
        note_book = NoteBook()
        note_book.add('alexey')
        note_book.add('alexander')

        self.assertEqual(note_book.find('sergey'), 2)
        self.assertEqual(note_book.find('sergey'), 0)

    def test1(self):
        note_book = NoteBook()
        note_book.add('alexey')
        note_book.add('alexander')

        self.assertEqual(note_book.find('alex'), 2)
        self.assertEqual(note_book.find('alek'), 0)


if __name__ == '__main__':
    unittest.main()


'''
O(n)
Способ хранения массив
'''