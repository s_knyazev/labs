from NoteBookTree.Node import Node


class Tree(object):
    pointer = None
    start_list = dict()

    def add(self, name):
        for ch in name:
            if self.pointer is None:
                if ch not in self.start_list:
                    node = Node()
                    self.start_list.update({ch: node})
                self.pointer = self.start_list.get(ch)
            else:
                children_list = self.pointer.get_children()

                if ch not in children_list:
                    node = Node()
                    children_list.update({ch: node})
                self.pointer = children_list.get(ch)

            self.pointer.inc_count(1)
        self.pointer = None

    def find(self, alpha_set):
        count = 0
        if alpha_set[0] in self.start_list:
            pointer = self.start_list.get(alpha_set[0])
            count = pointer.get_count()

            for ch in alpha_set[1:]:
                children = pointer.get_children()
                if children is not None and ch in children:
                    pointer = children.get(ch)
                    count = pointer.get_count()
                else:
                    count = 0
                    return count

        return count
