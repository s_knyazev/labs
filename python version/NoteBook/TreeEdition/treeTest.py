import unittest

from NoteBookTree.Tree import Tree


class TestTree(unittest.TestCase):

    @unittest.expectedFailure
    def test_broken(self):
        note_book = Tree()
        note_book.add('alexey')
        note_book.add('alexander')

        self.assertEqual(note_book.find('sergey'), 2)
        self.assertEqual(note_book.find('sergey'), 0)

    def test1(self):
        note_book = Tree()
        note_book.add('alexey')
        note_book.add('alexander')

        self.assertEqual(note_book.find('alex'), 2)
        self.assertEqual(note_book.find('alek'), 0)


if __name__ == '__main__':
    unittest.main()


'''
O(1)
Способ хранения дерево, храним каждую букву как узел, каждая последующая буква является ребенком текущей
40 min
'''