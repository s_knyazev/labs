

class Node(object):
    children = dict()
    i = 0

    def get_children(self):
        return self.children

    def inc_count(self, count):
        self.i += count

    def get_count(self):
        return self.i