import unittest

from src.minmaxfounder import MinMaxFounder


class TestFindMinMax(unittest.TestCase):

    def test1(self):
        text = "Today is very good a sunny day"
        word1 = 'day'
        word2 = 'Today'

        min_result, max_result = MinMaxFounder.find_min_and_max(text, word1, word2)
        self.assertEqual(min_result, 5)
        self.assertEqual(max_result, 5)

    def test2(self):
        text = 'Today is very good a sunny day and tomorrow will be very good day too.'
        word1 = 'day'
        word2 = 'Today'

        min_result, max_result = MinMaxFounder.find_min_and_max(text, word1, word2)
        self.assertEqual(min_result, 5)
        self.assertEqual(max_result, 12)


if __name__ == '__main__':
    unittest.main()

'''
O(n^2)
Затраченое время 20 мин
'''