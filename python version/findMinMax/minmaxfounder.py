import math


class MinMaxFounder(object):

    @staticmethod
    def find_min_and_max(text, word1, word2):
        words = text.split()
        i = 0

        w1 = list()
        w2 = list()
        for word in words:
            i += 1
            if word == word1:
                w1.append(i)
            if word == word2:
                w2.append(i)


        min_result = None
        max_result = None
        for i in w1:
            for j in w2:
                d = math.fabs(i - j - 1)
                if min_result is None:
                    min_result = d
                elif min_result > d:
                    min_result = d

                if max_result is None:
                    max_result = d
                elif max_result < d:
                    max_result = d

        return min_result, max_result