import org.junit.Assert;
import org.junit.Test;

public class testMain {
    @Test
    public void testMaxDistance1() {
        Main main = new Main();
        main.setText("Today is very good a sunny day");
        main.setFirstWord("day");
        main.setSecondWord("Today");
        main.process();
        Integer max = main.getMaxDistance();
        Assert.assertEquals(new Integer(5), max);
    }

    @Test
    public void testMaxDistance2() {
        Main main = new Main();
        main.setText("Today is very good a sunny day and tomorrow will be very good day too");
        main.setFirstWord("day");
        main.setSecondWord("Today");
        main.process();
        Integer max = main.getMaxDistance();
        Assert.assertEquals(new Integer(12), max);
    }

    @Test
    public void testMinDistance1() {
        Main main = new Main();
        main.setText("Today is very good a sunny day");
        main.setFirstWord("day");
        main.setSecondWord("Today");
        main.process();
        Integer min = main.getMinDistance();
        Assert.assertEquals(new Integer(5), min);
    }

    @Test
    public void testMin2() {
        Main main = new Main();
        main.setText("Today is very good a sunny day and tomorrow will be very good day too");
        main.setFirstWord("day");
        main.setSecondWord("Today");
        main.process();
        Integer min = main.getMinDistance();
        Assert.assertEquals(new Integer(5), min);
    }
}
