public class Main {

    private String _text;
    private String _secondWord;
    private String _firstWord;
    private Integer _maxDistance;
    private Integer _minDistance;

    public Main() {
        _text = "";
        _firstWord = "";
        _secondWord = "";
    }

    public void setText(String text) {
        _text = text.toLowerCase().trim();
    }

    public void setFirstWord(String firstWord) {
        _firstWord = firstWord.toLowerCase();
    }

    public void setSecondWord(String secondWord) {
        _secondWord = secondWord.toLowerCase();
    }

    public void process() {
        if(_text == null || _firstWord == null || _secondWord == null) {
            throw new IllegalArgumentException("One of the arguments(text || firstWord || secondWord) is not provided.");
        }

        String[] wordList = _text.split("\\s+");
        Integer i = 0;
        Integer firstWordFirstPosition = null;
        Integer firstWordLastPosition = null;
        Integer secondWordFirstPosition = null;
        Integer secondWordLastPosition = null;
        Integer min = wordList.length;
        Integer minBuffer = wordList.length;

        for(String word: wordList) {
            //init first and last positions of words
            if(word.equals(_firstWord)) {
                if(firstWordFirstPosition == null) {
                    firstWordFirstPosition = i;
                }
                firstWordLastPosition = i;
            }
            if(word.equals(_secondWord)) {
                if(secondWordFirstPosition == null) {
                    secondWordFirstPosition = i;
                }
                secondWordLastPosition = i;
            }

            //check min distance
            if(firstWordFirstPosition != null && secondWordFirstPosition != null) {
                minBuffer = secondWordLastPosition - firstWordLastPosition;
                if (minBuffer > 0 && min > minBuffer) {
                    min = minBuffer;
                }

                minBuffer = firstWordLastPosition - secondWordLastPosition;
                if (minBuffer > 0 && min > minBuffer) {
                    min = minBuffer;
                }
            }
            i++;
        }
        if(firstWordFirstPosition == null || secondWordFirstPosition == null) {
            throw new IllegalArgumentException("Some word was not found in the text.");
        }
        Integer maxDistance1 = secondWordLastPosition - firstWordFirstPosition;
        Integer maxDistance2 = firstWordLastPosition - secondWordFirstPosition;
        _maxDistance = (maxDistance1 > maxDistance2 ? maxDistance1 : maxDistance2) - 1;
        _minDistance = min - 1;
    }

    public Integer getMinDistance() {
        return _minDistance;
    }

    public Integer getMaxDistance() {
        return _maxDistance;
    }
}
