package Calculator;

import java.util.*;

public class Basket {
    private Map<Product, Integer> _products = new HashMap<>();
    private Integer productCount = 0;

    public void addProduct(Product product) {
        productCount++;

        if(_products.containsKey(product)) {
            _products.put(product, _products.get(product) + 1);
        } else {
            _products.put(product, 1);
        }
    }

    public Double calculateResult() {
        Double cost = 0.;

        //Rule 3
        cost += makeSale(new ArrayList<>(Arrays.asList(Product.A, Product.B)), 0.1);
        //Rule 4
        cost += makeSale(new ArrayList<>(Arrays.asList(Product.D, Product.E)), 0.05);

        double afterSaleCost = 0;
        //Rule 5
        afterSaleCost += makeSale(new ArrayList<>(Arrays.asList(Product.E, Product.F, Product.G)), 0.05);

        //Rule 6
        afterSaleCost += makeSale(new ArrayList<>(Arrays.asList(Product.A, Product.K)), 0.05);
        afterSaleCost += makeSale(new ArrayList<>(Arrays.asList(Product.A, Product.L)), 0.05);
        afterSaleCost += makeSale(new ArrayList<>(Arrays.asList(Product.A, Product.M)), 0.05);

        for(Map.Entry<Product, Integer> product : _products.entrySet()) {
            cost += product.getKey().getCost() * product.getValue();
        }

        double productCountSale = 1.;
        switch (productCount){
            case 3://Rule 7
                productCountSale = 0.95;
                break;
            case 4://Rule 8
                productCountSale = 0.9;
                break;
            case 5://Rule 9
                productCountSale = 0.8;
                break;
            default:
                if(productCount > 5) {
                    productCountSale = 0.8;
                }
        }

        cost *= productCountSale;
        cost += afterSaleCost;

        return cost;
    }

    public void clear() {
        _products.clear();
        productCount = 0;
    }

    private double makeSale(ArrayList<Product> products, Double saleInPercent) {
        Double sale = 1 - saleInPercent;
        Double cost = 0.;

        //if we haven't product in our list
        for (Product product : products) {
            if (!_products.containsKey(product)) {
                return cost;
            }
        }

        Integer delta = _products.values().stream().min(Integer::compare).get();

        for (Product product : products) {
            cost += product.getCost();
            _products.put(product, _products.get(product) - delta);
            if(_products.get(product) == 0) {
                _products.remove(product);
            }
        }
        return cost * delta * sale;
    }
}
