package Calculator;

public enum Product {
    A(10.),
    B(15.),
    C(20.),
    D(25.),
    E(30.),
    F(11.),
    G(12.),
    K(13.),
    L(14.),
    M(15.);

    private Double _cost;

    Product(Double cost){
        _cost = cost;
    }

    public Double getCost() {
        return _cost;
    }
}
