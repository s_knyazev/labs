package Discounts;

import Calculator.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class Discount {

    private Map<Product, Integer> _products = new HashMap<>();
    private Discount  wrapper;

    public Discount() {

    }

    public Discount(Discount discount) {
        wrapper = discount;
    }

    abstract public Double getCostAfterDiscount(Map<Product, Integer> _products);

    final protected Double makeSale(ArrayList<Product> products, Double saleInPercent) {
        Double sale = 1 - saleInPercent;
        Double cost = 0.;

        //if we haven't product in our list
        for (Product product : products) {
            if (!_products.containsKey(product)) {
                return cost;
            }
        }

        Integer delta = _products.values().stream().min(Integer::compare).get();

        for (Product product : products) {
            cost += product.getCost();
            _products.put(product, _products.get(product) - delta);
            if(_products.get(product) == 0) {
                _products.remove(product);
            }
        }
        return cost * delta * sale;
    }
}
