package Discounts;

import Calculator.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Rule3 extends Discount {
    @Override
    public Double getCostAfterDiscount(Map<Product, Integer> _products) {
        return super.makeSale(new ArrayList<>(Arrays.asList(Product.A, Product.B)), 0.1);
    }
}
