import Calculator.Basket;
import Calculator.Product;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestBasket extends Assert {
    private Basket basket = new Basket();

    @BeforeMethod
    public void setUp() {
        basket.clear();
    }
    @DataProvider
    public Object[][] prepareProductList() {
        return new Object[][]{
                {Product.A.getCost(), Collections.singletonList(Product.A)},//10
                {(Product.A.getCost() + Product.B.getCost()) * 0.9, Arrays.asList(Product.A, Product.B)},//Rule 3
                {(Product.D.getCost() + Product.E.getCost()) * 0.95, Arrays.asList(Product.D, Product.E)},//Rule 4
                {(Product.E.getCost() + Product.F.getCost() + Product.G.getCost()) * 0.95, Arrays.asList(Product.E, Product.F, Product.G)},//Rule 5

                {(Product.A.getCost() + Product.K.getCost()) * 0.95, Arrays.asList(Product.A, Product.K)},//Rule 6
                {(Product.A.getCost() + Product.L.getCost()) * 0.95, Arrays.asList(Product.A, Product.L)},//Rule 6
                {(Product.A.getCost() + Product.M.getCost()) * 0.95, Arrays.asList(Product.A, Product.M)},//Rule 6

                {(Product.A.getCost() * 3 * 0.95), Arrays.asList(Product.A, Product.A, Product.A)},//Rule 7
                {(Product.A.getCost() * 4 * 0.9), Arrays.asList(Product.A, Product.A, Product.A, Product.A)},//Rule 8
                {(Product.A.getCost() * 5 * 0.8), Arrays.asList(Product.A, Product.A, Product.A, Product.A, Product.A)},//Rule 9
        };
    }

    @Test(dataProvider = "prepareProductList")
    public void test(Double expected, List<Product> products) {
        for(Product product : products) {
            basket.addProduct(product);
        }
        Assert.assertEquals(basket.calculateResult(), expected);
    }

    @Test
    public void testExceptionRules() {
        //Rule 5
        basket.addProduct(Product.E);
        basket.addProduct(Product.F);
        basket.addProduct(Product.G);
        //Rule 6
        basket.addProduct(Product.A);
        basket.addProduct(Product.K);
        //Rule 7
        //none
        Double expectedRule5 = (Product.E.getCost() + Product.F.getCost() + Product.G.getCost()) * 0.95;
        Double expectedRule6 = (Product.A.getCost() + Product.K.getCost()) * 0.95;
        Double expected = expectedRule5 + expectedRule6;
        Assert.assertEquals(expected, basket.calculateResult());
    }
}
